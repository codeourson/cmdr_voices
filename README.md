# CMDR_Voices

Le logiciel open-source pour contrôler vos applications Windows ou vos jeux par la voix

Interdite pour une utilisation commerciale sans une autorisation écrite et manuscrite de ma part !

Auteur : Pierre-Aimé IMBERT

Debug : Jacques-Olivier IMBERT

Mail : pai.logicielslibres@gmail.com


# Installation

1) Télécharger l'archive zip (dans le dossier versions).

2) Extraire l'archive dans C:

3) Installer Jdk-10.2 et Perl Strawberry (.exe dans C:\CMDRVoices\dependences).

4) Lancer le fichier .jar pour l'utiliser.

# Informations techniques

Version 1.3.1 : correction de l'interface ( merci à Lénaïc Riédinger pour le partage et les remarques )

Moteur vocale par le logiciel open-source Sphinx4.

Gestion du clavier Windows par le logiciel open-source System-hook.

Version disponible en français.

Version anglaise en cours.
