package cmdr_voices;

import java.awt.AWTException;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;
import javax.swing.JCheckBox;
import java.awt.TextArea;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import java.awt.Label;
import java.awt.Robot;

import lc.kra.system.keyboard.GlobalKeyboardHook;
import lc.kra.system.keyboard.event.GlobalKeyAdapter;
import lc.kra.system.keyboard.event.GlobalKeyEvent;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;

public class mainFrame extends JFrame 
	{
	private JPanel contentPane;
	private JTextField textField1;
	private JTextField textField2;
	private JTextField textField3;
	private JTextField textField4;
	private JTextField textField5;
	private TextArea textArea1;
	private TextArea textArea2;
	private JComboBox comboBox1;
	private JComboBox comboBox2;
	private JComboBox comboBox3;
	private JComboBox comboBox4;
	private JComboBox comboBox5;
	private JComboBox comboBox6;
	private JLabel label3;
	private JLabel label11;
	private JLabel label13;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button6;
	private JButton button7;
	private JRadioButton radioButton1;
	private JRadioButton radioButton2;
	private JCheckBox checkBox1;
	private JCheckBox checkBox2;
	private JCheckBox checkBox3;
	
	private static GlobalKeyboardHook keyboardHook;
    private static int touchesHexa[] = 
        {
        KeyEvent.VK_BACK_SPACE,		KeyEvent.VK_TAB,		KeyEvent.VK_CLEAR,			KeyEvent.VK_ENTER,			KeyEvent.VK_PAUSE,
        KeyEvent.VK_CAPS_LOCK,		KeyEvent.VK_ESCAPE,		KeyEvent.VK_SPACE,			KeyEvent.VK_PAGE_UP,		KeyEvent.VK_PAGE_DOWN,	
        KeyEvent.VK_END, 			KeyEvent.VK_HOME,		KeyEvent.VK_LEFT,			KeyEvent.VK_UP,
        KeyEvent.VK_RIGHT,			KeyEvent.VK_DOWN,		KeyEvent.VK_PRINTSCREEN,	KeyEvent.VK_INSERT, 		KeyEvent.VK_DELETE,
        KeyEvent.VK_HELP,			KeyEvent.VK_0,			KeyEvent.VK_1,				KeyEvent.VK_2,				KeyEvent.VK_3,
        KeyEvent.VK_4,				KeyEvent.VK_5,			KeyEvent.VK_6,				KeyEvent.VK_7,				KeyEvent.VK_8,
        KeyEvent.VK_9,				KeyEvent.VK_A,			KeyEvent.VK_B,				KeyEvent.VK_C,				KeyEvent.VK_D,
        KeyEvent.VK_E,				KeyEvent.VK_F,			KeyEvent.VK_G,				KeyEvent.VK_H,				KeyEvent.VK_I,
        KeyEvent.VK_J,				KeyEvent.VK_K,			KeyEvent.VK_L,				KeyEvent.VK_M,				KeyEvent.VK_N,
        KeyEvent.VK_O,				KeyEvent.VK_P,			KeyEvent.VK_Q,				KeyEvent.VK_R,				KeyEvent.VK_S,
        KeyEvent.VK_T,				KeyEvent.VK_U,			KeyEvent.VK_V,				KeyEvent.VK_W,				KeyEvent.VK_X,
        KeyEvent.VK_Y,				KeyEvent.VK_Z,			KeyEvent.VK_WINDOWS,		KeyEvent.VK_CONTEXT_MENU,	KeyEvent.VK_NUMPAD0,
        KeyEvent.VK_NUMPAD1,		KeyEvent.VK_NUMPAD2,	KeyEvent.VK_NUMPAD3,		KeyEvent.VK_NUMPAD4,		KeyEvent.VK_NUMPAD5,
        KeyEvent.VK_NUMPAD6,		KeyEvent.VK_NUMPAD7,	KeyEvent.VK_NUMPAD8,		KeyEvent.VK_NUMPAD9,		KeyEvent.VK_SEPARATOR,
        KeyEvent.VK_F1,				KeyEvent.VK_F2,			KeyEvent.VK_F3,				KeyEvent.VK_F4,				KeyEvent.VK_F5,
        KeyEvent.VK_F6,				KeyEvent.VK_F7,			KeyEvent.VK_F8,				KeyEvent.VK_F9,				KeyEvent.VK_F10,
        KeyEvent.VK_F11,			KeyEvent.VK_F12,		KeyEvent.VK_F13,			KeyEvent.VK_F14,			KeyEvent.VK_F15,
        KeyEvent.VK_F16,			KeyEvent.VK_F17,		KeyEvent.VK_F18,			KeyEvent.VK_F19,			KeyEvent.VK_F20,
        KeyEvent.VK_F21,			KeyEvent.VK_F22,		KeyEvent.VK_F23,			KeyEvent.VK_F24,			KeyEvent.VK_NUM_LOCK,
        KeyEvent.VK_SCROLL_LOCK,	KeyEvent.VK_CONTROL,	KeyEvent.VK_ALT,			KeyEvent.VK_ALT_GRAPH
        };
	
	private static Configuration configuration;
	private static LiveSpeechRecognizer recognize;
    private static boolean run = false;
    private static boolean ready = false;
    private static boolean ordre = false;

	
	/*
	 * Lancement de l'application
	 */
	public static void main(String[] args) 
		{
		EventQueue.invokeLater(new Runnable() 
			{
			public void run() 
				{
				try 
					{
					mainFrame frame = new mainFrame();
					frame.setVisible(true);
					
					//Debug : routine test touches clavier programm�s
					//Robot ro=new Robot();
					//for(int k=0; k<touchesHexa.length;k++)
					//	{
					//	System.out.println("Test : " + touchesHexa[k]);
					//	ro.keyPress(touchesHexa[k]);
					//	ro.keyRelease(touchesHexa[k]);
					//	}
					
					//Debug : routine test touches clavier
					//Robot ro=new Robot();
					//ro.keyPress(KeyEvent.VK_ALT);
					//ro.keyPress(KeyEvent.VK_F4);
					//ro.keyRelease(KeyEvent.VK_ALT);
					//7keyRelease(KeyEvent.VK_F4);
					}
				
				catch (Exception e)
					{
					e.printStackTrace();
					}
				}
			});
		}

	
	/*
	 * Cr�ation de la fen�tre principale.
	 */
	public mainFrame() 
		{
		setTitle("CMDR Voices 1.3.1");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 900, 880);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button1 = new JButton("Cr\u00E9er le profil");
		button1.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant la cr�ation de profil et l'initialisation de la fenetre
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				//Si le nom du profil est nul, on ne fait rien et on affiche l'erreur
				if(textField1.getText().isEmpty())
					{
					System.out.println("Nom profil vide !");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Nom du profil vide. Veuillez saisir un nom...");
					}
				else
					{
					//Initialisation des composants de la fen�tre
					textArea1.setText("");
					textField2.setEnabled(true);
					textField2.setEditable(true);
					comboBox1.removeAllItems();
					comboBox2.removeAllItems();
				
					textField1.setText(textField1.getText()+".cmdru");
					try 
	            		{
						//Cr�ation des fichiers pour le profil
						PrintWriter writer = new PrintWriter("C:/CMDRVoices/profils/" + textField1.getText());    
						PrintWriter writer2 = new PrintWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt");
						PrintWriter writer3 = new PrintWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt.dic");
						PrintWriter writer4 = new PrintWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt.ordres");
						writer4.close();
						writer3.close();
						writer2.close();
						writer.close();
	            
						//Initialisation des composants
						textField3.setText("");
						button3.setEnabled(false);
						textArea1.setText("");
						
						//Rafraichissement des profils disponibles
						File dir= new File("C:/CMDRVoices/profils/");
						ArrayList<String> textFiles = new ArrayList<String>();

						for (File file : dir.listFiles()) 
			    			{
							if (file.getName().endsWith((".cmdru"))) 
								{
								//Pour chaque fichier .cmdru de d�tect�, le m�moriser
								textFiles.add(file.getName());
								}
			    			}

						StringBuilder sb = new StringBuilder();
						for (String s : textFiles)
			    			{
							//Des profils de d�tect�s?? Ajoutons-les
							comboBox1.setEnabled(true);
							comboBox1.addItem(s);
			    			}
						
						//Message d'�tat OK
						System.out.println("Profil " + textField1.getText() + " cr��.");
						label11.setForeground(Color.GREEN);
						label11.setText("Etat : Succ�s => Profil " + textField1.getText() + " cr��.");
	            		}	 
										
					catch (FileNotFoundException ex) 
	            		{
						//Message d'�tat ERREUR
						System.out.println("C:/CMDRVoices/profils/ inexistant");
						label11.setForeground(Color.RED);
						label11.setText("Etat : Erreur => Dosssier C:/CMDRVoices/profils introuvable");
	            		}
					}
				}
		});
		button1.setBounds(574, 11, 115, 23);
		contentPane.add(button1);
		
		button2 = new JButton("Ajouter \u00E0 mon dictionnaire pour ce profil");
		button2.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'ajouter le mot au ComboBox et le m�moriser dans le profil
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				//Ouverture de la biblioth�que FR
		        File file = new File("C:/CMDRVoices/bd/fr.dic");
		        
		        BufferedReader br;
		        String line="init";
		        boolean test = false ;
		        try 
		            {
		        	//Ouverture des fichiers de profils
		            FileWriter writer = new FileWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt.dic", true);
		            FileWriter writer2 = new FileWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt", true);
		            
		            //On recherche la phon�tique associ� au mot choisi
		            br = new BufferedReader(new FileReader(file));
		            while (line != null )
		                {
		                line = br.readLine();
		                if (line != null) 
		                    {
		                    String[] linearray = new String[] {line};
		                    String[] linearray2 = new String[] {textField2.getText() };
		                    linearray = line.split(" ");
		                    if(linearray[0].equalsIgnoreCase((linearray2[0])) || linearray[0].equalsIgnoreCase((linearray2[0])+"(2)"))
		                        {
		                        test = true;
		                        System.out.println(line);
		                        writer.append(line + "\n");
		                        }
		                    }
		                }
		            
		            //Ajout du mot au fichier profil et fermeture du fichier
		            writer2.append(textField2.getText() + "\n");
		            writer2.close();
		            writer.close();
		            br.close();
		            
		            //Ajout du mot dans le comboBox et l'activer
		            comboBox2.addItem(textField2.getText());
		            comboBox2.setEnabled(true);
		            
		            //Mise � jour de la librairie pour sphinx
		            String cmd = "C:\\Strawberry\\perl\\bin\\perl C:\\CMDRVoices\\dependences\\quick_lm.pl -s C:\\CMDRVoices\\profils\\" + textField1.getText() + ".txt";
		            Process p = Runtime.getRuntime().exec(cmd);
		            		            
					//Message d'�tat OK
					System.out.println("Mot " + textField2.getText() + " ajout�.");
					label11.setForeground(Color.GREEN);
					label11.setText("Etat : Succ�s => Mot " + textField2.getText() + " ajout�.");
		            } 
		        catch (FileNotFoundException ex) 
		            {
		        	//Message d'�tat ERREUR
		        	System.out.println("Mot " + textField2.getText() + " impossible � ajouter.");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Mot "+ textField2.getText() + " impossible � ajouter");
		            } 
		        catch (IOException ex) 
		            {
		        	//Message d'�tat ERREUR
		        	System.out.println("Mot " + textField2.getText() + " impossible � ajouter.");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Mot "+ textField2.getText() + " impossible � ajouter");
		            }

				}
			});
		button2.setEnabled(false);
		button2.setBounds(10, 72, 290, 23);
		contentPane.add(button2);
		
		button3 = new JButton("");
		button3.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'effacer l'ordre complet
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				textField3.setText("");
				button3.setEnabled(false);
				button6.setEnabled(false);
				}
			});
		button3.setIcon(new ImageIcon(mainFrame.class.getResource("/cmdr_voices/cross.png")));
		button3.setEnabled(false);
		button3.setBounds(510, 132, 23, 23);
		contentPane.add(button3);
		
		button4 = new JButton("");
		button4.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'effacer l'ordre des touches et commandes
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				textField4.setText("");
				button4.setEnabled(false);
				button6.setEnabled(false);
				}
		});
		button4.setIcon(new ImageIcon(mainFrame.class.getResource("/cmdr_voices/cross.png")));
		button4.setEnabled(false);
		button4.setBounds(851, 268, 23, 23);
		contentPane.add(button4);
		
		JButton button5 = new JButton("Ajouter la commande");
		button5.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'envoyer la commande Windows dans l'ordre
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				if (textField4.getText().isEmpty())
					{
					button4.setEnabled(true);
					textField4.setText("CmdDOS------" + textField5.getText());
					
					if(textField4.getText().isEmpty() == false && textField3.getText().isEmpty() == false) button6.setEnabled(true);
					else button6.setEnabled(false);
					}
				else
					{			
					textField4.setText(textField4.getText() + "------CmdDOS------" + textField5.getText());
				
					if(textField4.getText().isEmpty() == false && textField3.getText().isEmpty() == false) button6.setEnabled(true);
					else button6.setEnabled(false);
					}
				}
			});
		button5.setEnabled(false);
		button5.setBounds(554, 235, 170, 23);
		contentPane.add(button5);
		
		button6 = new JButton("Cr\u00E9er l'ordre pour ce profil");
		button6.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'ajouter les ordres completes dans la liste et dans le profil
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				//Inscription de l'ordre
				textArea1.append(textField3.getText() + "------" + textField4.getText( ) + "\n");	
				try 
					{
					FileWriter writer = new FileWriter("C:/CMDRVoices/profils/" + textField1.getText() + ".txt.ordres", true);
					writer.append(textField3.getText() + "------" + textField4.getText( ) + "\n");
					writer.close();
					} 
				catch (IOException ex) 
	            	{
					//Message d'�tat ERREUR
		        	System.out.println("Probl�me d'�criture dans votre profil.");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Probl�me d'�criture dans votre profil");
	            	}

				
				//Initialisation de l'interface
				textField3.setText("");
				textField4.setText("");
				textField5.setText("");
				button3.setEnabled(false);
				button4.setEnabled(false);
				button6.setEnabled(false);
				
	        	//Message d'�tat OK
	        	System.out.println("Votre ordre a �t� ajout� � la liste de votre profil.");
				label11.setForeground(Color.GREEN);
				label11.setText("Etat : Succ�s => Votre ordre a �t� ajout� � la liste de votre profil.");
				}
			});
		button6.setEnabled(false);
		button6.setBounds(274, 299, 290, 23);
		contentPane.add(button6);
		
		button7 = new JButton("Activer le micro");
		button7.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant d'activer le micro via les donn�es du profil
			 */
			public void actionPerformed(ActionEvent arg0) 
				{			
	            configuration = new Configuration();
	            configuration.setAcousticModelPath("file:///C:/CMDRVoices/fr-fr");
	            configuration.setDictionaryPath("file:///C:/CMDRVoices/profils/" + textField1.getText() + ".txt.dic");
	            configuration.setLanguageModelPath("file:///C:/CMDRVoices/profils/" + textField1.getText() + ".txt.lm");
	            try 
	            	{
	            	recognize = new LiveSpeechRecognizer(configuration);
					//Lancement du d�mon de d�tection de clavier
					keyboardHook = new GlobalKeyboardHook(false);
			        System.out.println("Global keyboard hook successfully started.");
			        
					/*
					 * Sous-programme permettant de g�rer les actions lors de la demande vocale
					 */
			        keyboardHook.addKeyListener(new GlobalKeyAdapter()
		            	{
		                @Override public void keyPressed(GlobalKeyEvent event)
		                	{
		                	System.gc();
		                    int keymic = event.getVirtualKeyCode();
		                    int mic = touchesHexa[comboBox4.getSelectedIndex()];
		                    System.out.println(keymic + " " + mic);
		                    if (keymic == mic && run == false && ready == true)
		                    	{                           	
		                        recognize.startRecognition(true);
		                        System.out.println("Speech recognition will be launched");
		                        run = true;
		       
		                        //Create SpeechResult Object
		                        SpeechResult result;

		                        //Get the recognize speech
		                        while((result = recognize.getResult()) == null);  
		                        String command = result.getHypothesis();
		            
		                        int i = 0;
		                        ordre = false;
		                        File file = new File("C:/CMDRVoices/profils/" + textField1.getText() + ".txt.ordres");
		                        BufferedReader br;
		                        String line="init";
		                        
		                        try
		                        	{
		                            br = new BufferedReader(new FileReader(file));
		                            while (line != null )
		                            	{
		                                line = br.readLine();
		                                if (line != null) 
		                                	{
		                                    String[] linearray = new String[] {line};
		                                    String[] linearray2 = new String[] {command};
		                                    System.out.println(command);
		                                    System.out.println(linearray[0]);
		                                    linearray = line.split("------");
		                                    if(linearray[0].equals(linearray2[0]))
		                                    	{
		                                    	Robot robot= new Robot();
		                                        robot.keyPress(mic);
		                                        robot.delay(20);
		                                        robot.keyRelease(mic);
		                                        robot.delay(20);
		                                        System.out.println("Ordre reconnu dans le profil : " + linearray[0] + "\n");
		                                        textArea2.append("V : Ordre reconnu dans le profil : " + linearray[0] + "\n");
		                                        
		                                        int size=linearray.length;
		                                        for (int j=1;j<size;j++)
		                                        	{
		                                        	if(linearray[j].equals("ClavierPO"))
		                                        		{
		                                        		comboBox5.setSelectedItem((String) linearray[j+1]);
		                                        		robot.keyPress(touchesHexa[comboBox5.getSelectedIndex()]);
		                                        		robot.delay(Integer.parseInt(linearray[j+2]));
		                                        		}
		                                        	else if(linearray[j].equals("ClavierRO"))
	                                        			{
		                                        		comboBox5.setSelectedItem((String) linearray[j+1]);
		                                        		robot.keyRelease(touchesHexa[comboBox5.getSelectedIndex()]);
		                                        		robot.delay(Integer.parseInt(linearray[j+2]));
	                                        			}
		                                        	else if(linearray[j].equals("ClavierPR"))
	                                        			{
		                                        		comboBox5.setSelectedItem((String) linearray[j+1]);
	                                        			robot.keyPress(touchesHexa[comboBox5.getSelectedIndex()]);
	                                        			robot.delay(Integer.parseInt(linearray[j+2]));
	                                        			robot.keyRelease(touchesHexa[comboBox5.getSelectedIndex()]);
	                                        			robot.delay(Integer.parseInt(linearray[j+2]));
	                                        			}
		                                        	else if(linearray[j].equals("CmdDOS"))
			                                        	{
			                                            String cmd = linearray[j+1];
			                                            Process p = Runtime.getRuntime().exec(cmd);
			                                        	}
		                                        	}
		                                        ordre=true;
		                                        line=null;
		                                    	}
		                                	}
		                            	}
		                            	br.close();
		                        	}
		                        catch (FileNotFoundException ex)
		                        	{
		                           
		                        	} 
		                        catch (IOException ex) 
		                        	{
		                           
		                        	} 
		                        catch (AWTException e) 
		                        	{
		                        	
		                        	}
		             
		                        if (ordre == false)
		                        	{
		                            System.out.println("Ordre non reconnu...");
		                            textArea2.append("E : Ordre non reconnu...\n");
		                            Robot robot;
									try 
										{
										robot = new Robot();
										robot.keyPress(mic);
			                            robot.delay(100);
			                            robot.keyRelease(mic);
			                            robot.delay(100);
										} 
									catch (AWTException e) 
										{
										// TODO Auto-generated catch block
										e.printStackTrace();
										}
		                        	} 
		                        recognize.stopRecognition();
		                    	}
		                    else if (keymic == mic && run == true) 
		                    	{
		                        run = false ;
		                        System.out.println("Reconnaissance vocale refus�e ou occup�e ou non accord�e...");    
		                    	}
		                	}
		            	});
					
					//Cloture des composants de l'interface
					button1.setEnabled(false);
					button2.setEnabled(false);
					button3.setEnabled(false);
					button4.setEnabled(false);
					button5.setEnabled(false);
					button6.setEnabled(false);
					button7.setEnabled(false);
					checkBox1.setEnabled(false);
					comboBox1.setEnabled(false);
					comboBox2.setEnabled(false);
					comboBox3.setEnabled(false);
					radioButton1.setEnabled(false);
					radioButton2.setEnabled(false);
					textField1.setEnabled(false);
					textField2.setEnabled(false);
					textField3.setEnabled(false);
					textField4.setEnabled(false);
					textField5.setEnabled(false);
					label3.setText("");
					
		        	//Message d'�tat OK
					ready=true;
		        	System.out.println("Activation de la reconnaissance vocale. Red�marrez l'application si vous souhaitez changer de profil.");
					label11.setForeground(Color.GREEN);
					label11.setText("Etat : Succ�s => Activation de la reconnaissance vocale. Red�marrez l'application si vous souhaitez changer de profil.");
	            	} 
	            catch (IOException e) 
	            	{
		        	//Message d'�tat ERREUR
		        	System.out.println("Activation de la reconnaissance vocale impossible (souci avec votre micro ou votre profil ?)");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Activation de la reconnaissance vocale impossible (souci avec votre micro ou votre profil ?)");
	            	}
				}
			});
		button7.setBounds(584, 299, 290, 23);
		contentPane.add(button7);
		
		checkBox1 = new JCheckBox("Maintenir");
		checkBox1.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant de g�rer le checkbox
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				checkBox2.setSelected(false);
				checkBox3.setSelected(false);
				}
			});
		checkBox1.setBackground(Color.WHITE);
		checkBox1.setBounds(106, 205, 86, 23);
		contentPane.add(checkBox1);
			
		comboBox1 = new JComboBox();
		comboBox1.addPopupMenuListener(new PopupMenuListener() 
			{
			public void popupMenuCanceled(PopupMenuEvent arg0) 
				{
				//Rien
				}
			/*
			 * Sous-programme permettant de lire les profils
			 */
			public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) 
				{
				//Lecture des donn�es
		        File file = new File("C:/CMDRVoices/profils/" + comboBox1.getSelectedItem() + ".txt");
		        File file2 = new File("C:/CMDRVoices/profils/" + comboBox1.getSelectedItem() + ".txt.ordres");
		        
		        //Mise a jour du profil choisi
		        textField1.setText((String) comboBox1.getSelectedItem());
		        textField2.setEnabled(true);
		        textField2.setEditable(true);
		        
		        //Initialisation des composants
		        textArea1.setText("");
		        comboBox2.removeAllItems();
		        comboBox2.setEnabled(true);
		        button3.setEnabled(false);
				textArea1.setText("");
				textField3.setText("");
		        
		        //Lecture de la biblioth�que de mots pour ce profil et de la liste d'ordre
		        BufferedReader br;
		        String line="init";
		        try 
		            {
		            br = new BufferedReader(new FileReader(file));
		            while (line != null )
		                {
		                line = br.readLine();
		                if (line != null) 
		                    {
		                    comboBox2.addItem(line);
		                    }
		                }
		            br.close();
		            
		            line="init";
		            br = new BufferedReader(new FileReader(file2));
		            while (line != null )
		                {
		                line = br.readLine();
		                if (line != null) 
		                    {
		                    textArea1.append(line+"\n");
		                    }
		                }
		            br.close();
		            
					//Message d'�tat OK
					System.out.println("Profil " + textField1.getText() + " charg�.");
					label11.setForeground(Color.GREEN);
					label11.setText("Etat : Succ�s => Profil " + textField1.getText() + " charg�.");
		            }
		        catch (FileNotFoundException ex) 
		            {
					//Message d'�tat ERREUR
					System.out.println("Profil "+ comboBox1.getSelectedItem() + " corrompu ou inexistant");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Profil "+ comboBox1.getSelectedItem() + " corrompu ou inexistant");
					textField2.setEnabled(false);
					textField2.setEditable(false);
		            } 
		        catch (IOException ex) 
		            {
					//Message d'�tat ERREUR
					System.out.println("Profil "+ comboBox1.getSelectedItem() + " corrompu ou inexistant");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Profil "+ comboBox1.getSelectedItem() + " corrompu ou inexistant");
					textField2.setEnabled(false);
					textField2.setEditable(false);
		            }

				}
			public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) 
				{
				//Rien
				}
			});
		
		checkBox2 = new JCheckBox("Rel\u00E2cher");
		checkBox2.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant de g�rer le checkbox
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				checkBox1.setSelected(false);
				checkBox3.setSelected(false);
				}
			});
		checkBox2.setBackground(Color.WHITE);
		checkBox2.setBounds(194, 205, 90, 23);
		contentPane.add(checkBox2);
		
		checkBox3 = new JCheckBox("Maintenir/Rel\u00E2cher");
		checkBox3.setSelected(true);
		checkBox3.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant de g�rer le checkbox
			 */
			public void actionPerformed(ActionEvent arg0) 
				{
				checkBox1.setSelected(false);
				checkBox2.setSelected(false);
				}
			});
		checkBox3.setBackground(Color.WHITE);
		checkBox3.setBounds(286, 205, 133, 23);
		contentPane.add(checkBox3);
		comboBox1.setBounds(714, 11, 160, 22);
		contentPane.add(comboBox1);
		//Rafraichissement des profils disponibles
		File dir= new File("C:/CMDRVoices/profils/");
		ArrayList<String> textFiles = new ArrayList<String>();

		for (File file : dir.listFiles()) 
    		{
			if (file.getName().endsWith((".cmdru"))) 
				{
				//Pour chaque fichier .cmdru de d�tect�, le m�moriser
				textFiles.add(file.getName());
				}
    		}

		StringBuilder sb = new StringBuilder();
		for (String s : textFiles)
    		{
			//Des profils de d�tect�s?? Ajoutons-les
			comboBox1.setEnabled(true);
			comboBox1.addItem(s);
    		}
				
		comboBox2 = new JComboBox();
		comboBox2.addPopupMenuListener(new PopupMenuListener() 
			{
			public void popupMenuCanceled(PopupMenuEvent arg0) 
				{
				//Rien
				}
			/*
			 * Sous-programme permettant de cr�er l'ordre � prononcer
			 */
			public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) 
				{		
				if(textField3.getText().isEmpty())
					{
					button3.setEnabled(true);
					String s= (String) comboBox2.getSelectedItem();
					textField3.setText(s);
					}
				else
					{
					String s= (String) textField3.getText() + " " + (String) comboBox2.getSelectedItem();
					textField3.setText(s);
					}
				
				if(textField4.getText().isEmpty() == false && textField3.getText().isEmpty() == false) button6.setEnabled(true);
				else button6.setEnabled(false);
				}
			public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) 
				{
				//Rien
				}
			});
		comboBox2.setEnabled(false);
		comboBox2.setBounds(10, 132, 160, 22);
		contentPane.add(comboBox2);
		
		comboBox3 = new JComboBox();
		comboBox3.addPopupMenuListener(new PopupMenuListener() 
			{
			public void popupMenuCanceled(PopupMenuEvent arg0) 
				{
				//Rien
				}
			/*
			 * Sous-programme permettant d'ajouter les ordres de touches-clavier
			 */
			public void popupMenuWillBecomeInvisible(PopupMenuEvent arg0) 
				{
				if (textField4.getText().isEmpty())
					{
					button4.setEnabled(true);
					
					if(checkBox1.isSelected())
						{
						textField4.setText("ClavierPO------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					else if (checkBox2.isSelected())
						{
						textField4.setText("ClavierRO------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					else
						{
						textField4.setText("ClavierPR------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					
					if(textField4.getText().isEmpty() == false && textField3.getText().isEmpty() == false) button6.setEnabled(true);
					else button6.setEnabled(false);
					}
				else
					{			
					if(checkBox1.isSelected())
						{
						textField4.setText(textField4.getText() + "------ClavierPO------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					else if (checkBox2.isSelected())
						{
						textField4.setText(textField4.getText() + "------ClavierRO------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					else
						{
						textField4.setText(textField4.getText() + "------ClavierPR------" + (String) comboBox3.getSelectedItem() + "------" + (String) comboBox6.getSelectedItem());
						}
					
					if(textField4.getText().isEmpty() == false && textField3.getText().isEmpty() == false) button6.setEnabled(true);
					else button6.setEnabled(false);
					}
				}
			public void popupMenuWillBecomeVisible(PopupMenuEvent arg0) 
				{
				//Rien
				}
			});
		comboBox3.setModel(new DefaultComboBoxModel(new String[] {"Backspace", "Tab", "Clear", "Enter", "Pause", "Caps Lock", "Esc", "Space", "Page Up", "Page Down", "End", "Home", "Arrow Left", "Arrow Up", "Arrow Right", "Arrow Down", "Print Screen", "Insert", "Delete", "Help", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Windows", "Context Menu", "Numpad 0", "Numpad 1", "Numpad 2", "Numpad 3", "Numpad 4", "Numpad 5", "Numpad 6", "Numpad 7", "Numpad 8", "Numpad 9", "Separator", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21", "F22", "F23", "F24", "Num Lock", "Scroll Lock", "Control", "Alt", "Alt Graph"}));
		comboBox3.setBounds(160, 235, 170, 22);
		contentPane.add(comboBox3);
		
		comboBox4 = new JComboBox();
		comboBox4.setModel(new DefaultComboBoxModel(new String[] {"Backspace", "Tab", "Clear", "Enter", "Pause", "Caps Lock", "Esc", "Space", "Page Up", "Page Down", "End", "Home", "Arrow Left", "Arrow Up", "Arrow Right", "Arrow Down", "Print Screen", "Insert", "Delete", "Help", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Windows", "Context Menu", "Numpad 0", "Numpad 1", "Numpad 2", "Numpad 3", "Numpad 4", "Numpad 5", "Numpad 6", "Numpad 7", "Numpad 8", "Numpad 9", "Separator", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21", "F22", "F23", "F24", "Num Lock", "Scroll Lock", "Control", "Alt", "Alt Graph"}));
		comboBox4.setBounds(149, 521, 115, 22);
		contentPane.add(comboBox4);
		
		comboBox5 = new JComboBox();
		comboBox5.setModel(new DefaultComboBoxModel(new String[] {"Backspace", "Tab", "Clear", "Enter", "Pause", "Caps Lock", "Esc", "Space", "Page Up", "Page Down", "End", "Home", "Arrow Left", "Arrow Up", "Arrow Right", "Arrow Down", "Print Screen", "Insert", "Delete", "Help", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "Windows", "Context Menu", "Numpad 0", "Numpad 1", "Numpad 2", "Numpad 3", "Numpad 4", "Numpad 5", "Numpad 6", "Numpad 7", "Numpad 8", "Numpad 9", "Separator", "F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "F19", "F20", "F21", "F22", "F23", "F24", "Num Lock", "Scroll Lock", "Control", "Alt", "Alt Graph"}));
		comboBox5.setEnabled(false);
		comboBox5.setBounds(738, 812, 136, 22);
		contentPane.add(comboBox5);
		comboBox5.setVisible(false);
		
		comboBox6 = new JComboBox();
		comboBox6.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "5", "10", "15", "20", "25", "50", "75", "100", "150", "200", "250", "500", "750", "1000", "2000", "3000", "4000", "5000", "6000", "7000", "8000", "9000"}));
		comboBox6.setBounds(426, 205, 56, 22);
		contentPane.add(comboBox6);
		
		JLabel label1 = new JLabel("1\u00B0) Donnez un nom pour votre profil ou chargez le :");
		label1.setFont(new Font("Tahoma", Font.BOLD, 15));
		label1.setBounds(10, 11, 384, 19);
		contentPane.add(label1);
		
		JLabel label2 = new JLabel("2\u00B0) �crivez votre commande vocale d�sir�e :");
		label2.setFont(new Font("Tahoma", Font.BOLD, 15));
		label2.setBounds(10, 47, 349, 19);
		contentPane.add(label2);
		
		label3 = new JLabel("R\u00E9sultat : Attente de votre mot...");
		label3.setFont(new Font("Tahoma", Font.ITALIC, 11));
		label3.setBounds(539, 51, 335, 14);
		contentPane.add(label3);
		
		JLabel label4 = new JLabel("3\u00B0) Composez votre ordre vocal avec les mots que vous avez enregistr\u00E9 :");
		label4.setFont(new Font("Tahoma", Font.BOLD, 15));
		label4.setBounds(10, 106, 549, 19);
		contentPane.add(label4);
		
		JLabel label5 = new JLabel("B\u00B0) Touche(s) clavier :");
		label5.setBounds(26, 239, 144, 14);
		contentPane.add(label5);
		
		JLabel label6 = new JLabel("A\u00B0) Options :");
		label6.setBounds(26, 209, 74, 14);
		contentPane.add(label6);
		
		JLabel label7 = new JLabel("CMDR Voices version 1.3.1");
		label7.setHorizontalAlignment(SwingConstants.CENTER);
		label7.setFont(new Font("Tahoma", Font.BOLD, 15));
		label7.setBounds(10, 299, 254, 19);
		contentPane.add(label7);
		
		JLabel lable8 = new JLabel("par Pierre-Aim\u00E9 IMBERT");
		lable8.setHorizontalAlignment(SwingConstants.CENTER);
		lable8.setFont(new Font("Tahoma", Font.BOLD, 15));
		lable8.setBounds(10, 319, 254, 19);
		contentPane.add(lable8);
		
		JLabel label9 = new JLabel("Debug du logiciel");
		label9.setHorizontalAlignment(SwingConstants.CENTER);
		label9.setFont(new Font("Tahoma", Font.BOLD, 15));
		label9.setBounds(10, 366, 254, 19);
		contentPane.add(label9);
		
		JLabel label10 = new JLabel("par Jacques-Olivier IMBERT");
		label10.setHorizontalAlignment(SwingConstants.CENTER);
		label10.setFont(new Font("Tahoma", Font.BOLD, 15));
		label10.setBounds(10, 388, 254, 19);
		contentPane.add(label10);
		
		label11 = new JLabel("Etat : Aucun profil charg\u00E9 !");
		label11.setBackground(Color.WHITE);
		label11.setFont(new Font("Tahoma", Font.BOLD, 11));
		label11.setForeground(Color.ORANGE);
		label11.setBounds(10, 816, 719, 14);
		contentPane.add(label11);
		
		radioButton1 = new JRadioButton("Mode Clavier");
		radioButton1.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant de v�rifier que le mode Clavier est s�lectionn�
			 */	
			public void actionPerformed(ActionEvent arg0) 
				{
				checkBox1.setEnabled(true);
				checkBox2.setEnabled(true);
				checkBox3.setEnabled(true);
				comboBox3.setEnabled(true);
				comboBox6.setEnabled(true);
				radioButton2.setSelected(false);
				textField5.setEnabled(false);
				textField5.setEditable(false);
				button5.setEnabled(false);
				}
			});
		
		Label label12 = new Label("Touche pour le micro :");
		label12.setBounds(10, 525, 133, 22);
		contentPane.add(label12);
		
		label13 = new JLabel("Logs de la reconnaissance vocale :");
		label13.setHorizontalAlignment(SwingConstants.CENTER);
		label13.setFont(new Font("Tahoma", Font.BOLD, 15));
		label13.setBounds(10, 554, 257, 19);
		contentPane.add(label13);
		radioButton1.setBackground(Color.WHITE);
		radioButton1.setSelected(true);
		radioButton1.setBounds(6, 179, 109, 23);
		contentPane.add(radioButton1);
		
		radioButton2 = new JRadioButton("Commande Windows");
		radioButton2.addActionListener(new ActionListener() 
			{
			/*
			 * Sous-programme permettant de v�rifier que le mode Commande Windows est s�lectionn�
			 */	
			public void actionPerformed(ActionEvent arg0) 
				{
				checkBox1.setEnabled(false);
				checkBox2.setEnabled(false);
				checkBox3.setEnabled(false);
				comboBox3.setEnabled(false);
				comboBox6.setEnabled(false);
				radioButton1.setSelected(false);
				textField5.setEnabled(true);
				textField5.setEditable(true);
				button5.setEnabled(true);
				}
			});
		radioButton2.setBackground(Color.WHITE);
		radioButton2.setBounds(554, 179, 170, 23);
		contentPane.add(radioButton2);
		
		textArea1 = new TextArea();
		textArea1.setEditable(false);
		textArea1.setBounds(274, 328, 600, 219);
		contentPane.add(textArea1);
		
		textArea2 = new TextArea();
		textArea2.setEditable(false);
		textArea2.setBounds(10, 579, 864, 231);
		contentPane.add(textArea2);
		
		textField1 = new JTextField();
		textField1.setBounds(404, 12, 160, 20);
		contentPane.add(textField1);
		textField1.setColumns(10);
		
		textField2 = new JTextField();
		textField2.addKeyListener(new KeyAdapter() 
			{
			/*
			 * Sous-programme permettant de v�rifier que le mot existe dans le dictionnaire et autorise � l'inscrire ensuite
			 */			
			public void keyReleased(KeyEvent arg0) 
				{
				//Chargement de la biblioth�que FR
				File file = new File("C:/CMDRVoices/bd/fr.dic");
		        
				BufferedReader br;
		        String line="init";
		        boolean test = false ;
		        label3.setText("Recherche du mot " + textField2.getText() + " en cours...");
		        
		        try 
		            {
		            br = new BufferedReader(new FileReader(file));
		            while (line != null )
		                {
		                line = br.readLine();
		                if (line != null) 
		                    {
		                    String[] linearray = new String[] {line};
		                    String[] linearray2 = new String[] {textField2.getText() };
		                    linearray = line.split(" ");
		                    //System.out.printf(linearray[0] + " " + linearray2[0] + " \n");
		                    if(linearray[0].equalsIgnoreCase((linearray2[0])) || linearray[0].equalsIgnoreCase((linearray2[0])+"(2)"))
		                        {
		                        test = true;
		                        System.out.println(line);
		                        }
		                    }
		                }
		            br.close();
		            if (test == true)
		                {
		                label3.setText("Occurrence trouv�e dans le dictionnaire !");
		                button2.setEnabled(true);
		                }
		            else
		                {
		                label3.setText("Introuvable dans le dictionnaire.");
		                button2.setEnabled(false);
		                }
		            }
		        catch (IOException ex) 
		            {
					//Message d'�tat ERREUR
					System.out.println("Fichier C:/CMDRVoices/bd/fr.dic inexistant");
					label11.setForeground(Color.RED);
					label11.setText("Etat : Erreur => Fichier C:/CMDRVoices/bd/fr.dic introuvable");
		            } 

				}
		});
		textField2.setEditable(false);
		textField2.setEnabled(false);
		textField2.setColumns(10);
		textField2.setBounds(369, 48, 160, 20);
		contentPane.add(textField2);
		
		textField3 = new JTextField();
		textField3.setEditable(false);
		textField3.setEnabled(false);
		textField3.setColumns(10);
		textField3.setBounds(180, 133, 320, 20);
		contentPane.add(textField3);
		
		textField4 = new JTextField();
		textField4.setEditable(false);
		textField4.setEnabled(false);
		textField4.setColumns(10);
		textField4.setBounds(10, 268, 831, 20);
		contentPane.add(textField4);
		
		textField5 = new JTextField();
		textField5.setEditable(false);
		textField5.setEnabled(false);
		textField5.setColumns(10);
		textField5.setBounds(554, 204, 320, 20);
		contentPane.add(textField5);
		
		JLabel lblMs = new JLabel("ms");
		lblMs.setBounds(487, 209, 23, 14);
		contentPane.add(lblMs);
		
		JLabel label14 = new JLabel("https://gitlab.com/Pierllos/CMDR_Voices");
		label14.setHorizontalAlignment(SwingConstants.CENTER);
		label14.setBounds(10, 434, 254, 14);
		contentPane.add(label14);
		}
	}
